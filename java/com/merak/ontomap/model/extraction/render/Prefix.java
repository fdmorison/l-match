// Package
///////////////
package com.merak.ontomap.model.extraction.render;

// Public Class
///////////////
public class Prefix {

  //~ Constants ////////////////////////////////////////////////////////////////
  //****************************************************************************
  public static final String PROPERTY_NAME  = "PN";
  public static final String PROPERTY_VALUE = "PV";
  public static final String PROPERTY_TYPE  = "PT";
  public static final String CLASS_NAME     = "CN";
  public static final String DIRECT_CLASS   = "DC";
  public static final String SUPER_CLASS    = "SC";
  public static final String INSTANCE_NAME  = "IN";

  //****************************************************************************

}
