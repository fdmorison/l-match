// Package
///////////////
package com.merak.ontomap.model.extraction;

// Imports
///////////////
import java.io.*;
import java.util.*;
import com.merak.core.*;

// Public Class
///////////////
public class NewInstance extends IdentifiedNamedObject {

  //~ Attributes ///////////////////////////////////////////////////////////////
  //****************************************************************************
  //~ Constructors /////////////////////////////////////////////////////////////
  //****************************************************************************
  public NewInstance(int id,String name) {

    // Super Attribute Initialization
    super(id,name);

  }

  //~ Methods //////////////////////////////////////////////////////////////////
  //****************************************************************************

}