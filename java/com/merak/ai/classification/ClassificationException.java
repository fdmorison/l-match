// Package
///////////////
package com.merak.ai.classification;

public class ClassificationException extends Exception {

  //~ Constructors /////////////////////////////////////////////////////////////
  /****************************************************************************/
  public ClassificationException(String msg) {

    super(msg);

  }
  /****************************************************************************/
  public ClassificationException(String msg,Throwable cause) {

    super(msg,cause);

  }
  /****************************************************************************/

}
