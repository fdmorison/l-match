/**
 * AWT Sample application
 *
 * @author 
 * @version 1.00 08/07/15
 */
public class Ontomap {
    
    public static void main(String[] args) {
        // Create application frame.
        OntomapFrame frame = new OntomapFrame();
        
        // Show frame
        frame.setVisible(true);
    }
}
